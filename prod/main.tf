provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_instance" "newprod" {
  ami = "ami-01ed306a12b7d1c96"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.sgprod.id]
  key_name = "prodkey"
  subnet_id = "${aws_subnet.public_subnet.id}"
  user_data = "${file("create_users.sh")}"

  tags = {
    Name = "new-prod"
  }

}

resource "aws_vpc" "ljvpc" {
  cidr_block = "10.10.0.0/16"
  
  enable_dns_support = true
  enable_dns_hostnames = true
  
  tags = {Name="LJVPC"}
  #tags {
  #  Name = "LJ VPC"
  #}
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.ljvpc.id}"
}

resource "aws_route" "internet_access" {
  route_table_id          = "${aws_vpc.ljvpc.main_route_table_id}"
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = "${aws_internet_gateway.gw.id}"
}

resource "aws_subnet" "public_subnet" {
  vpc_id                  = "${aws_vpc.ljvpc.id}"
  cidr_block              = "10.10.1.0/24" # 10.10.1.0 - 10.10.1.255 (256)
  map_public_ip_on_launch = true
}

resource "aws_subnet" "private_subnet" {
  vpc_id                  = "${aws_vpc.ljvpc.id}"
  cidr_block              = "10.10.16.0/20" # 10.10.16.0 - 10.0.31.255 (4096)
  map_public_ip_on_launch = true
}

resource "aws_security_group" "sgprod" {
  name = "terraform-lj-sg"
  vpc_id      = "${aws_vpc.ljvpc.id}"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["192.168.1.0/25"]
  }
  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 4100
    to_port     = 4100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5100
    to_port     = 5100
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 7000
    to_port     = 7000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 58500
    to_port     = 58500
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}
